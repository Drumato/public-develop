# books.go

BookManageの機能を**Goで実装したもの**｡  
処理速度がかなり早くなっている｡  

Linux-x64(64ビット),Linux-x86(32ビット)と､  
Windows(上記と同じ)に対してクロスコンパイルしたバイナリを置いてあります｡  

** `books.csv` **というファイル名の書籍データを､  
バイナリと**同階層** に置いて頂ければ利用可能です｡  

Bashコマンドに登録するには(恐らく絶対パスで指定する必要がある)ので､  
Goの環境構築をしてから`books.go`の`filename`を変更してください｡  




# **[BookManage](https://gitlab.com/Drumato/public-develop/tree/master/bookmanage)**

書籍学習を主体に独学で勉強している私が､  
｢今までやった本をまとめたい｣と思って作ったツール｡  

CSVファイルの独特な記法を気にせず､  
自分のやった本を**綺麗な表形式**で表示できます｡  

依存gem:`terminal-table`

```terminal
$gem install terminal-table
```

すれば利用可能です｡

プログラムは

```termninal
ruby books.rb
```

として起動します｡

## Usage

主な機能は次のとおりです｡

- `append`…新しいレコードを追加します｡
- `view`…起動時に出力された**テーブルをもう一度**出力します｡
- `introduce`…ツールの使い方を紹介します｡

**[詳細はこちらから](https://drumato.hatenablog.com/entry/2018/09/30/123624)**わかります!
