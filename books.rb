require 'csv'
require 'terminal-table'
require 'color_echo/get'

class BookManage 
  HEADERS = %w(書名 難易度 おすすめ度 カテゴリ)
  TABLEPATH = 'books.csv'
  TITLE = 'TECHBOOK-DB'

  def initialize
    puts self.view
  end

  def action(method)
    self.send(method)
  end

  def append
    changestar = ->(input){
      return input if input.to_i <= 0
      return '*' * input.to_i
    }
    record = []
    HEADERS.each do |header|
      puts "#{header}を入力してください｡(日本語は使えません!)"
      factor = changestar[gets.chomp]
      record << factor
    end
    CSV.open(TABLEPATH,'a+') do |csv|
      csv << record
      puts ''
    end
  end

  def make_table(title='',info2d=[[]],**options)
    table = Terminal::Table.new(title:title,rows:info2d,headings:options[:header])
    return table
  end

  def view
    booktable = CSV.read(TABLEPATH)
    booktable.sort!{|before,after| before[3] <=> after[3]}
    return make_table(title=TITLE,info2d=booktable,header:HEADERS)
  end

  
  def introduce
    manual = [
      ['TECHBOOK-DBのマニュアルページへようこそ!'],
      ['このシステムは､読了した技術書を記録するCSVファイルを管理します｡'],
      ['書名には､読了した本の名前が登録されています｡'],
      ['難易度は､1~3までの*マークで表します｡'],
      ['おすすめ度も1~3の*マークです｡'],
      ['カテゴリは,言語名やツール名など､ひと目で分かるものが登録されています｡']
    ]
    return make_table(title='DBシステムマニュアル',info2d=manual,header:['システム概要'])
  end
end

def ui
  manager = BookManage.new
  loop do
    puts '利用したい機能を選択してください｡'
    puts 'レコード追加:append マニュアル表示:introduce CSVファイル表示:view システム終了:exit'
    func = gets.chomp
    exit! if func == 'exit'
    abort(CE.fg(:red).get('ERR! No such function')) unless manager.methods.include?(func.to_sym)
    puts manager.action(func)
  end
end

if __FILE__ == $0
  ui
end
