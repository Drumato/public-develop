package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"path"
	"sort"
	"strconv"
	"strings"

	"github.com/olekukonko/tablewriter"
)

func main() {
	var filename string
	file := fileRead(filename)
	var option string
	if len(os.Args) >= 2 {
		option = os.Args[1] //可読性の為の名前付け
	}
	switch option {
	case "-a":
		writer := getWriter(file, path.Ext(filename))
		fmt.Println("半角スペース区切りで書籍名､難易度､おすすめ度､カテゴリーを入力してください｡")
		input := scanByStdin()
		record := createRecord(input)
		writer.Write(record)
		writer.Flush()
	case "--category":
		fmt.Println("カテゴリー名を入力してください｡")
		reader := getReader(file, path.Ext(filename))
		category := scanByStdin()
		table := make2dTable(reader)
		table = filterByCategory(table, category)
		renderTableStdout(table)
	default:
		reader := getReader(file, path.Ext(filename))
		table := make2dTable(reader)
		renderTableStdout(table)
	}

}

func fileRead(filename string) *os.File {
	filename = "/home/drumato/env/go/bookmanage/books.csv"
	fp, err := os.OpenFile(filename, os.O_RDWR|os.O_APPEND, 0644)
	if err != nil {
		errorLog(err)
	}
	return fp
}

func getReader(fp *os.File, ext string) *csv.Reader {
	reader := csv.NewReader(fp)
	if ext == "tsv" {
		reader.Comma = '\t'
	}
	reader.LazyQuotes = true
	return reader
}

func getWriter(fp *os.File, ext string) *csv.Writer {
	writer := csv.NewWriter(fp)
	if ext == "tsv" {
		writer.Comma = '\t'
	}
	return writer
}

func scanByStdin() string {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	input := scanner.Text()
	return input
}

func createRecord(input string) []string {
	record := strings.Split(input, " ")
	for i, value := range record[1:3] {
		record[i+1] = parseNumberToStar(value)
	}
	return record
}

func parseNumberToStar(value string) string {
	amount, _ := strconv.Atoi(value)
	return strings.Repeat("*", amount)
}

func make2dTable(reader *csv.Reader) [][]string {
	var table [][]string
	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		} else {
			table = append(table, record)
		}
	}
	sort.SliceStable(table, func(i, j int) bool { return table[i][3] < table[j][3] })
	return table
}

func filterByCategory(beforetable [][]string, category string) [][]string {
	var aftertable [][]string
	for _, record := range beforetable {
		if record[3] == category {
			aftertable = append(aftertable, record)
		}
	}
	return aftertable
}

func renderTableStdout(table [][]string) {
	writer := tablewriter.NewWriter(os.Stdout)
	writer.SetHeader([]string{"書籍名", "難易度", "おすすめ度", "カテゴリ"})
	for _, record := range table {
		writer.Append(record)
	}
	writer.Render()
}

func errorLog(err error) {
	log.Fatal(err)
	os.Exit(1)
}
