package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"unsafe"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"golang.org/x/crypto/bcrypt"
)

const (
	dbpath = "books.db"
)

type Book struct {
	Id             int    `json:id sql:"primary_key"`
	Name           string `json:name`
	Difficulty     int    `json:difficulty`
	RecommendPoint int    `json:reccomendpoint`
}

func main() {
	fmt.Println("passwordを入力してください｡")
	pass := scanByStdin()

	fp := fileRead()
	if hash, err := ioutil.ReadAll(fp); err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		os.Exit(1)
	} else {
		if err := bcrypt.CompareHashAndPassword(bytes.TrimRight(hash, "\n"), stringToBytes(pass)); err != nil {
			fmt.Fprintf(os.Stderr, "%v\n", err)
			os.Exit(1)
		}
	}
	db := gormConnect()
	defer db.Close()

	book := Book{}
	book.Id = 1
	book.Name = "example"
	book.Difficulty = 1
	book.RecommendPoint = 1

	db.Create(&book)
	db.Find(&book)
}

func scanByStdin() string {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	input := scanner.Text()
	return input
}

func gormConnect() *gorm.DB {

	DBMS := "sqlite3"

	db, err := gorm.Open(DBMS, dbpath)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		os.Exit(1)
	}
	return db
}

func fileRead() *os.File {
	filename := "/home/drumato/env/go/bookmanage/password.txt"
	fp, err := os.OpenFile(filename, os.O_RDONLY, 0600)
	if err != nil {
		fmt.Fprintf(os.Stderr, "can't open file\n")
		os.Exit(1)
	}
	return fp
}

func stringToBytes(s string) []byte {
	return *(*[]byte)(unsafe.Pointer(&s))
}
